# arg-parsing

This is the code for this question on stackoverflow.com:

https://stackoverflow.com/questions/68915694/gnu-c-arg-parser-options-with-required-arguments-are-parsed-wrongly

## Question

I'm learning [GNU C Arg Parser][1] and it parses options with required arguments in a strange way. 

Consider this example from the documentation:
https://www.gnu.org/software/libc/manual/html_node/Argp-Example-3.html

    $./a --help       
    Usage: a [OPTION...] ARG1 ARG2
    Argp example #3 -- a program with options and arguments using argp
    
     -o, --output=FILE          Output to FILE instead of standard output
     -q, -s, --quiet, --silent  Don't produce any output
     -v, --verbose              Produce verbose output
     -?, --help                 Give this help list
         --usage                Give a short usage message
     -V, --version              Print program version

`-o / --output` requires argument `FILE`. 

Now, if you omit `FILE` and put the next option behind it, like this

    ./a -o -v arg1 arg2
    ARG1 = arg1
    ARG2 = arg2
    OUTPUT_FILE = -v
    VERBOSE = no
    SILENT = no

it will take `-v` as `FILE`. I would expect the above command to fail because there was no option for `-o`. `-v` should be IMHO considered to be the next option and not the argument for `-o`. 

Is this a bug or am I missing something?

Consider this similar code in Python:

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--foo', help='foo help')
    args = parser.parse_args()

If I execute `python3 a.py --foo -h`, it will recognize that the mandatory argument for `--foo` is missing and it will not take `-h` as an argument for `--foo`. 

    python3 a.py --foo -h
    usage: a.py [-h] [--foo FOO]
